CC=gcc
CFLAGS=-std=c99 -Wall -fprofile-arcs -ftest-coverage
TESTFLAGS=-fprofile-arcs -ftest-coverage

.PHONY: build test clean

build: testcomparaison testhasard testapp

test: testcomparaison
	./testcomparaison

clean:
	rm -f *.o testcomparaison testhasard testapp

testcomparaison: testcomparaison.o comparaison.o
	$(CC) $(CFLAGS) -o testcomparaison testcomparaison.o comparaison.o

testhasard: testhasard.o hasard.o
	$(CC) $(CFLAGS) -o testhasard testhasard.o hasard.o

testapp: testapp.o hasard.o comparaison.o
	$(CC) $(CFLAGS) $(TESTFLAGS) -o testapp testapp.o hasard.o comparaison.o

testapp.o: testapp.c hasard.h comparaison.h hasard.c comparaison.c
	$(CC) $(CFLAGS) $(TESTFLAGS) -c $< -o $@

run-testapp: testapp
	./testapp R P C R R C C P P

coverage: run-testapp
	gcov -c -p testapp
